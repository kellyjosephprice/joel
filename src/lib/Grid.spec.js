import _ from 'lodash'

import { CubeCoord, HexGrid, SquareCoord } from './Grid.js'

const makeGrid = (string) => {
  const grid = _.chain(string)
    .trim()
    .split("\n")
    .map(line => (
      [...line.matchAll(/\d+/g)].map(m => parseInt(m[0]))
    ))
    .value()

  return new HexGrid({ grid })
}

const testCell = ({ coord, expected, grids }) => {
  grids
    .map(grid => makeGrid(grid))
    .forEach((grid) => {
      grid.tick()

      expect(grid.valueAt(coord)).toEqual(expected)
    })
}

describe(HexGrid, () => {
  test(".getNeighbors(x, y)", () => {
    const grid = new HexGrid()

    let neighbors = grid.getNeighbors(0, 0).map(c => c.toArray())
    expect(neighbors.sort()).toEqual([
      [1, 0],
      [0, 1],
    ].sort())

    neighbors = grid.getNeighbors(1, 1).map(c => c.toArray())
    expect(neighbors.sort()).toEqual([
      [1, 0],
      [2, 0],
      [2, 1],
      [2, 2],
      [1, 2],
      [0, 1],
    ].sort())
  })

  describe(".tick() -- Joe Life rules", () => {
    test("if a cell is currently dead it comes to life as juvenile if sum is exactly 4", () => {
      const grid = makeGrid(`
         0 1 0 0
          1 0 2 0
         0 0 0 0
          0 0 0 0
      `)
      grid.tick()

      expect(grid.valueAt([1, 1])).toEqual(1)
    })

    test("if the cell is juvenile it goes to adult if sum is 1,2,3,4,6", () => {
      const grids = [
        `
          0 1 0 0
           0 1 0 0
          0 0 0 0
           0 0 0 0
       `, `
          0 0 0 0
           0 1 0 0
          0 2 0 0
           0 0 0 0
       `, `
          0 0 0 0
           1 1 1 0
          0 1 0 0
           0 0 0 0
       `, `
          0 2 0 0
           0 1 1 0
          0 1 0 0
           0 0 0 0
       `, `
          0 1 1 0
           2 1 1 0
          0 0 1 0
           0 0 0 0
        `
      ]

      testCell({ grids, expected: 2, coord: [1, 1] })
    })

    test("if the cell is an adult, it stays adult if the sum is 1 or 2", () => {
      const grids = [
        `
          0 1 0 0
           0 2 0 0
          0 0 0 0
           0 0 0 0
        `, `
          0 0 0 0
           0 2 0 0
          0 2 0 0
           0 0 0 0
        `
      ]

      testCell({ grids, expected: 2, coord: [1, 1] })
    })

    test("if the cell is an adult, it reverts to juvenile if it the sum is 4", () => {
      const grids = [
        `
          0 1 0 0
           0 2 2 0
          0 0 1 0
           0 0 0 0
        `
      ]

      testCell({ grids, expected: 1, coord: [1, 1] })
    })

    test("arbitrary grid", () => {
      const grid = makeGrid(`
        0 1 2 1
         1 2 2 0
        2 1 2 2
         0 1 0 1
      `)

      grid.tick()

      expect(grid.grid).toEqual(makeGrid(`
        0 0 0 2
         2 0 0 0
        2 0 0 0
         1 2 0 2
      `).grid)

      grid.tick()

      expect(grid.grid).toEqual(makeGrid(`
        0 0 0 0
         2 0 0 0
        0 0 0 0
         2 2 1 0
      `).grid)
    })
  })

  test(".toString()", () => {
    const grid = new HexGrid()
    let expected = `\
0 0 0 0
 0 0 0 0
0 0 0 0
 0 0 0 0`
    expect(grid.toString()).toEqual(expected)

    grid.setValue({ x: 0, y: -1, z: 1 }, 2)
    expected = `\
0 0 0 0
 2 0 0 0
0 0 0 0
 0 0 0 0`
    expect(grid.toString()).toEqual(expected)
  })
})

describe(CubeCoord, () => {
  test(".getNeighbors()", () => {
    let coord = new CubeCoord(0, 0 ,0)

    expect(coord.getNeighbors().map(n => n.toArray())).toEqual([
      [-1, 1, 0],
      [1, -1, 0],
      [-1, 0, 1],
      [1, 0, -1],
      [0, 1, -1],
      [0, -1, 1],
    ])

    coord = new CubeCoord(0, -1, 1)

    expect(coord.getNeighbors().map(n => n.toArray()).sort()).toEqual([
      [0, 0 ,0],
      [1, -1, 0],
      [1, -2, 1],
      [0, -2, 2],
      [-1, -1, 2],
      [-1, 0, 1],
    ].sort())
  })

  test(".toSquareCoord()", () => {
    let coord = new CubeCoord(0, 0, 0)
    expect(coord.toSquareCoord().toArray()).toEqual([0, 0])

    coord = new CubeCoord(1, -2, 1)
    expect(coord.toSquareCoord().toArray()).toEqual([1, 1])
  })
})

describe(SquareCoord, () => {
  test(".toCubeCoord()", () => {
    let coord = new SquareCoord(0, 0)
    expect(coord.toCubeCoord().toArray()).toEqual([0, 0, 0])

    coord = new SquareCoord(1, 1)
    expect(coord.toCubeCoord().toArray()).toEqual([1, -2, 1])
  })
})
