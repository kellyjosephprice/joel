import _ from 'lodash'

const OFFSETS = [
  [-1, 1, 0],
  [1, -1, 0],
  [-1, 0, 1],
  [1, 0, -1],
  [0, 1, -1],
  [0, -1, 1],
]

class CubeCoord {
  constructor(x, y, z) {
    this.x = x
    this.y = y
    this.z = z
  }

  getNeighbors() {
    return OFFSETS.map(([xOffset, yOffset, zOffset]) => (
      new this.constructor(this.x + xOffset, this.y + yOffset, this.z + zOffset)
    ))
  }

  toArray() {
    return [this.x, this.y, this.z]
  }

  toSquareCoord() {
    const x = this.x + (this.z - (this.z & 1)) / 2
    const y = this.z

    return new SquareCoord(x, y)
  }
}

class SquareCoord {
  static asArray(coord) {
    let [x, y] = []

    if (coord instanceof SquareCoord) {
      ({ x, y } = coord)
    } else {
      [x, y] = coord
    }

    return [x, y]
  }

  constructor(x, y) {
    this.x = x
    this.y = y
  }

  toCubeCoord() {
    const x = this.x - (this.y - (this.y & 1)) / 2
    const z = this.y
    let y = -x - z
    // XXX
    if (y === 0) y = 0

    return new CubeCoord(x, y, z)
  }

  toArray() {
    return [this.x, this.y]
  }

  toObject() {
    return { x: this.x, y: this.y }
  }
}

class HexGrid {
  static defaultRuleset = "B4/A12346/R4/S12"
  static actions = {
    B: { from: 0, to: 1 }, // Birth
    A: { from: 1, to: 2 }, // Age
    R: { from: 2, to: 1 }, // Regress
    S: { from: 2, to: 2 }, // Survive
  }

  static parseRules = (ruleset) => {
    const rules = ruleset.split('/')
      .map((string) => string.split(''))
      .map(([action, ...sums]) => ([action, ...sums.map(s => parseInt(s))]))

    return (value, sum) => {
      let nextValue

      const found = rules.find(([action, ...sums]) => {
        if (this.actions[action].from == value && sums.includes(sum)) {
          return nextValue = this.actions[action].to
        }
      })

      if (!found) { nextValue = 0 }

      return nextValue
    }
  }

  constructor({ grid, size = 4, rules = this.constructor.defaultRuleset } = {}) {
    this.size = size
    this.grid = grid || Array.from(Array(this.size), () => Array.from(new Array(this.size),  () => 0))
    this.rules = this.constructor.parseRules(rules)
  }

  clone() {
    return new this.constructor({ grid: this.grid.map(row => row.slice()) })
  }

  getNeighbors(x, y) {
    return new SquareCoord(x, y).toCubeCoord().getNeighbors()
      .map(coord => coord.toSquareCoord())
      .filter(coord => {
        return coord.x >= 0 && coord.x < this.size && coord.y >= 0 && coord.y < this.size
      })
  }

  tick() {
    const next = this.clone()

    this.grid.forEach((row, y) => {
      row.forEach((value, x) => {
        const sum = this.getNeighbors(x, y)
          .map(coord => this.valueAt(coord))
          .reduce((m, n) => m + n, 0)
        const nextValue = this.rules(value, sum)

        next.setValue({ x, y }, nextValue)
      })
    })

    this.grid = next.grid

    return this
  }

  valueAt(coord) {
    let [x, y] = SquareCoord.asArray(coord)
    return this.grid[y][x]
  }

  setValue({ x, y, z }, value) {
    if (typeof z === "undefined") {
      this.grid[y][x] = value
    } else {
      this.setValue(new CubeCoord(x, y, z).toSquareCoord().toObject(), value)
    }
  }

  toString() {
    return this.grid.map((row, y) => {
      const offset = y & 1 ? " " : ""
      return offset + row.map((cell, x) => cell).join(" ")
    }).join("\n")
  }
}

export {
  CubeCoord,
  SquareCoord,
  HexGrid,
}
