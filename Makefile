IMAGE_NAME := joel
VOLUMES := -v $(PWD):/app \
	-v /app/node_modules

$(IMAGE_NAME) := docker run --rm -it $(VOLUMES) $(1) $(IMAGE_NAME)

all: test

.PHONY: image
image:
	docker build -t $(IMAGE_NAME) .

.PHONY: shell
shell: image
  $(call $(IMAGE_NAME)) bash

.PHONY: serve
serve: image
	$(call $(IMAGE_NAME))

.PHONY: test
test: image
	$(call $(IMAGE_NAME)) yarn test
